const express = require('express');
const cors = require('cors');
const path = require('path');
const DBUtils = require('./DBUtils');

const app = express();

app.use(cors());

app.use(express.json());

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/package.json')
})

app.post('/login', function (req, res) {
    console.log(req.body)
    DBUtils.getUser(req.body)
})

app.listen(3000);