import Vue from 'vue'
import axios from 'axios'

import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
import './assets/css/main.css'

Vue.config.productionTip = false

let Axios = axios.create({
  baseURL:'http://localhost:3000/'
})

Vue.prototype.a = Axios;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
